﻿#pragma strict
var target : GameObject;
private var drawed : boolean = true;
private var aiming : boolean = false;
private var playerview : boolean = true;

function Start(){

}

function OnGUI () {
		if (GUI.Button(Rect(10,10,50,25),"fire"))
			fire();

		if (GUI.Button(Rect(70,10,70,25),"burst fire"))
			burstfire();
		
		if (GUI.Button(Rect(150,10,115,25),"Aim in/out"))
			aim();	
			
		if (GUI.Button(Rect(275,10,70,25),"reload"))
			reload();	
			
		if (GUI.Button(Rect(355,10,85,25),"holster/draw"))
			draw();	
			
		if (GUI.Button(Rect(595,10,130,25),"player/world model"))
			switchview();
	}

function fire () {
target.GetComponent.<Animation>().Play("singleShot");
target.GetComponent.<Animation>().CrossFade("idle",0.4);
}

function burstfire () {
target.GetComponent.<Animation>().Play("autoShot");
}

function aim () {
if (!aiming) {
	target.GetComponent.<Animation>()["aim"].speed = 0.9;
	target.GetComponent.<Animation>().Play("aim");
	aiming=true;
	}
else {
	//animation["aimOUT"].speed = 0.9;
	target.GetComponent.<Animation>().Play("aimOut");
	aiming=false;
	//target.animation.CrossFade("idle",1);
	}
}


function reload () {
//animation["reload"].speed = 1.5;
target.GetComponent.<Animation>().Play("reload");
//target.animation.CrossFade("idle",7);
}

function draw () {

if (drawed) {
	//animation["holster"].speed = 1.5;
	target.GetComponent.<Animation>().Play("holster");
	drawed=false;
	}
else {
	//animation["draw"].speed = 1.5;
	target.GetComponent.<Animation>().Play("draw");
	drawed=true;
	target.GetComponent.<Animation>().CrossFade("idle",1.5);
	}

}






function switchview () {
	if(playerview){
		gameObject.Find("mp5kWorldHD").GetComponent.<Renderer>().enabled=true;
		gameObject.Find("hands").GetComponent.<Renderer>().enabled=false;
		gameObject.Find("mp4k09_bullet").GetComponent.<Renderer>().enabled=false;
		gameObject.Find("mp4k09_charger").GetComponent.<Renderer>().enabled=false;
		gameObject.Find("mp4k09_eject").GetComponent.<Renderer>().enabled=false;
		gameObject.Find("mp4k09_ring").GetComponent.<Renderer>().enabled=false;
		gameObject.Find("mp4k09_mp4k").GetComponent.<Renderer>().enabled=false;
		
		playerview = false;
	}
	else{
		gameObject.Find("mp5kWorldHD").GetComponent.<Renderer>().enabled=false;
		gameObject.Find("hands").GetComponent.<Renderer>().enabled=true;
		gameObject.Find("mp4k09_bullet").GetComponent.<Renderer>().enabled=true;
		gameObject.Find("mp4k09_charger").GetComponent.<Renderer>().enabled=true;
		gameObject.Find("mp4k09_eject").GetComponent.<Renderer>().enabled=true;
		gameObject.Find("mp4k09_ring").GetComponent.<Renderer>().enabled=true;
		gameObject.Find("mp4k09_mp4k").GetComponent.<Renderer>().enabled=true;
		playerview = true;
	}
}