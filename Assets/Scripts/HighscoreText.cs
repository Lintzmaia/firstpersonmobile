﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighscoreText : MonoBehaviour {

	void Start () {
		GetComponent<Text> ().text = "Highscore \n" + PlayerPrefs.GetInt ("HighScore");
	}

}
