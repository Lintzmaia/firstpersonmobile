﻿using UnityEngine;
using System.Collections;

public class CallUpdateStatus : MonoBehaviour {

	UnlockGun[] UnlockList;

	// Use this for initialization
	void Start () {
		UnlockList = (UnlockGun[])FindObjectsOfType<UnlockGun> ();
	}
	
	public void UpdateStatus(){
		Debug.Log (gameObject.name);
		foreach (UnlockGun Ul in UnlockList) {
			
			Ul.UpdateStatus ();
		}
	}

}
