﻿using UnityEngine;
using System.Collections;

public class AIPathFindTest : MonoBehaviour {
	public GameObject Player;
	public GameObject ZombieTest;
	Camera Cam;
	void Start () {
		Cam = GetComponent<Camera> ();
		originalRotation = transform.parent.localRotation;
	}
	float rotationX;
	float rotationY;
	Quaternion originalRotation;
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit Hit;
			if (Physics.Raycast (Cam.ScreenPointToRay (Input.mousePosition), out Hit)) {
				Player.transform.position = Hit.point;
			}
		}
		if (Input.GetMouseButtonDown (1)) {
			RaycastHit Hit;
			if (Physics.Raycast (Cam.ScreenPointToRay (Input.mousePosition), out Hit)) {
				ZombieTest.transform.position = Hit.point;
				ZombieTest.GetComponent<AIMovement> ().FollowPlayer = true;
			}
		}
		if (Input.GetMouseButton (2)) {

			rotationX += (Input.GetAxis ("Mouse X") * 2);
			rotationY += (Input.GetAxis ("Mouse Y") * 2);


			Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
			Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, Vector3.left);

			transform.localRotation = Quaternion.Slerp(transform.localRotation, originalRotation * yQuaternion, 5 * Time.deltaTime);
			transform.parent.localRotation = Quaternion.Slerp(transform.parent.localRotation, originalRotation * xQuaternion, 5 * Time.deltaTime);
		}

		transform.localPosition += new Vector3 (0, 0, Input.mouseScrollDelta.y);

	}
}
