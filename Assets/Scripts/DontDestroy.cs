﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour {
	public static DontDestroy instance;

	void Awake(){
		if (DontDestroy.instance == null) {			
			DontDestroy.instance = this;
			DontDestroyOnLoad (this.gameObject);
		} else if(DontDestroy.instance != this) {
			Destroy (gameObject);
		}
	}
}
