﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class CameraControl : MonoBehaviour {
	public float Sensibility = 100;

	bool StopTranslation;
	bool TranslateRotation;

	float XClamp = 360;
	float YClamp = 88;
	float rotationX = 0;
	float rotationY = 0;

	public Text SensitivetyValue;
	public Scrollbar SenScrollBar;


	Quaternion originalRotation;

	int Inversion;

	private bool IsPointerOverUIObject(Vector2 PosToCheck) {		
		PointerEventData eventDataCurrentPosition = new PointerEventData (EventSystem.current);
		eventDataCurrentPosition.position = PosToCheck;

		List<RaycastResult> results = new List<RaycastResult> ();
		EventSystem.current.RaycastAll (eventDataCurrentPosition, results);	

		foreach (RaycastResult R in results) {
			if (R.gameObject.name != "WaveIndicator") {
				return true;
			}
		}	
		return false;
	}

	public void AjustSensibility(float Value){
		
		Sensibility = Mathf.Round (10 + Value * 70);
		
		PlayerPrefs.SetFloat ("Sensibility", Sensibility);
		SensitivetyValue.text = Sensibility.ToString ();
	}


	void Start(){
		Sensibility = PlayerPrefs.GetFloat ("Sensibility", 55);
		SenScrollBar.value = (Sensibility - 10) / 70;
		SensitivetyValue.text = Sensibility.ToString ();
		Inversion = 1;
		originalRotation = Quaternion.Euler (transform.localRotation.x, transform.parent.localRotation.y, 0);
		/*transform.parent.localRotation = originalRotation;
		transform.localRotation = Quaternion.Euler (0, 0, 0);*/
	}



	Vector2 TouchZeroPos;
	bool HasTouched;
	int FingerIndex;
	int previousCount;

	public Text DebugBox;

	void Update () {

		if (!HasTouched) {
			if (Input.touchCount > previousCount && Input.touchCount > 0) {
				for (int pass = 0; pass < Input.touchCount; pass++) {
					if (Input.touches [pass].position.x > Screen.width / 4 && Input.touches [pass].phase == TouchPhase.Began) {
						if (!IsPointerOverUIObject (Input.touches [pass].position)) {
							FingerIndex = Input.touches [pass].fingerId;
							HasTouched = true;
						}
						break;				
					}
				}
			}
		}

		if (HasTouched) {
			for (int pass = 0; pass < Input.touchCount; pass++) {
				if (Input.touches [pass].fingerId == FingerIndex) {
					if (Input.touches [pass].phase == TouchPhase.Moved) {
						rotationX += Inversion * (Input.touches [pass].deltaPosition.x * Sensibility * Time.deltaTime);
						rotationY += Inversion * (Input.touches [pass].deltaPosition.y * Sensibility * Time.deltaTime);

						rotationY = ClampAngle (rotationY, -YClamp, YClamp);
						rotationX = ClampAngle (rotationX, -XClamp, XClamp);

						Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
						Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, Vector3.left);

						transform.localRotation = originalRotation * yQuaternion;
						transform.parent.localRotation = originalRotation * xQuaternion;
					}
					if (Input.touches [pass].phase == TouchPhase.Ended) {
						HasTouched = false;
					}
					break;
				}
			}
		}




		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown (0) && !IsPointerOverUIObject(new Vector2( Input.mousePosition.x, Input.mousePosition.y))) {
			
			TranslateRotation = true;
			Cursor.visible = false;
		}

		if (Input.GetMouseButtonDown (1)) {
			TranslateRotation = false;
			Cursor.visible = true;
		}

		if (TranslateRotation) {
			

			rotationX += Inversion * (Input.GetAxis ("Mouse X") * Sensibility);
			rotationY += Inversion * Input.GetAxis ("Mouse Y") * Sensibility;

			rotationY = ClampAngle (rotationY, -YClamp, YClamp);
			rotationX = ClampAngle (rotationX, -XClamp, XClamp);

			Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
			Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, Vector3.left);

			transform.localRotation = Quaternion.Slerp(transform.localRotation, originalRotation * yQuaternion, 5 * Time.deltaTime);
			transform.parent.localRotation = Quaternion.Slerp(transform.parent.localRotation, originalRotation * xQuaternion, 5 * Time.deltaTime);
		}

		#endif


		previousCount = Input.touchCount;
	}

	float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp (angle, min, max);
	}

	public void InvertCamera(){
		Inversion *= -1;
	}
}