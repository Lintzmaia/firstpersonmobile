﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GunController : MonoBehaviour {
	public static GunController Manager;

	[Serializable]
	public class GunInfo
	{
		public int GunBaseDamage;
		public int MaxBulletsOnClip;
		public int StartBulletsOnClip;
		public int StartBulletsPool;
		public float ReloadTime;
		public float ReloadSoundDelay;
		public bool IsBurstFire;
		public float BurstFireTime;
		public bool IsShotgun;
		public bool IsPistol;
		public string UpgradePref;
		public AudioClip[] GunSounds;

		public void Values(int Gbs, int Mboc, int Sboc, int Sbp, float Rt, float Rsd, bool Ibf, float Bft, bool Isg, bool Ip, string Up, AudioClip[] Gs){
			GunBaseDamage = Gbs;
			MaxBulletsOnClip = Mboc;
			StartBulletsOnClip = Sboc;
			StartBulletsPool = Sbp;
			ReloadTime = Rt;
			Rsd = ReloadSoundDelay;
			IsBurstFire = Ibf;
			BurstFireTime = Bft;
			IsShotgun = Isg;
			UpgradePref = Up;
			IsPistol = Ip;
			GunSounds = Gs;
		}
	}

	int GunDamage;

	bool IsGun;

	public bool SpecialBurst;

	public GunInfo thisGun;

	public Text BulletPoolLabel;
	public Text BulletOnGunLabel;
	public Canvas GameCanvas;
	public Canvas GameOverCanvas;
	public Text ScoreLabel;
	public Canvas PauseCanvas;
	
	public Spawn ZombieSpawn;
	int MaxBulletsOnClip;
	AudioClip[] GunSound;


	float ReloadTime;
	public int MaxScore;
	float ReloadSoundDelay;
	float TimeBeteweenBurst;
	public LayerMask ZombieLayer;
	Camera Cam;
	AudioSource Source;
	Animation Anim;
	int BulletsPool;
	int BulletsOnGun;

	bool BurstFire;
	bool Shotgun;

	public AudioSource ShotgunCrack;

	void Awake(){
		Manager = this;
	}

	Gun_Preview gunScript;



	void Start () {
		BurstFire = thisGun.IsBurstFire;
		Shotgun = thisGun.IsShotgun;

		GunDamage = thisGun.GunBaseDamage;
		MaxBulletsOnClip = thisGun.MaxBulletsOnClip;
		gunScript = GetComponent<Gun_Preview> ();
		ZombieSpawn = Spawn.Spawner;
		MaxScore = ZombieSpawn.Round * 2;
		Cam = GetComponent<Camera> ();
		Source = GetComponentInChildren<AudioSource> ();
		BulletsPool = thisGun.StartBulletsPool;
		BulletsOnGun = thisGun.StartBulletsOnClip;

		BulletOnGunLabel.text = BulletsOnGun.ToString ();

		BulletPoolLabel.text = BulletsPool.ToString ();
		Anim = GetComponentInChildren<Animation> ();

		GunSound = thisGun.GunSounds;

		IsGun = thisGun.IsPistol;

		ReloadTime = thisGun.ReloadTime;
		ReloadSoundDelay = thisGun.ReloadSoundDelay;
		TimeBeteweenBurst = thisGun.BurstFireTime;
		/*if (PlayerPrefs.GetInt (thisGun.UpgradePref, 0) == 1) {
			GunDamage = GunDamage * 2;
		}*/
		Health = 10;
		Score = 0;
		Source.volume = 0.9f;



	}

	bool CanFire = true;
	int fingerIndex;

	public float TimeTest;

	bool CancelReloadInvoke;
	void Update () {


		/*
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Time.timeScale = 1;
			SceneManager.LoadScene (1);
		}
		*/
		#if UNITY_EDITOR
		if (Input.GetKeyUp (KeyCode.Mouse0)) {
			if (CanFire) {
				if (isFiring) {
					fingerIndex = -1;
					isFiring = false;
					if (Anim.IsPlaying ("autoShot") || Anim.IsPlaying("shotBurst") || Anim.IsPlaying("Gun03_ShootAuto") || Anim.IsPlaying("Gun01_ShootAuto") && !Anim.IsPlaying ("reload") || !Anim.IsPlaying("Gun01Reload") || !Anim.IsPlaying("Gun03Reload")) {
						gunScript.StopBurrstFire ();
					}
				}
			}

		}
		if (Input.GetKeyDown (KeyCode.R)) {
			Reload (false);
		}

		if (BurstFire) {
			if (!isFiring) {
				if (CanFire) {
					
					if (Input.GetKeyDown (KeyCode.Mouse0)) {
						FireMachine (false);
					}

				}
			}
		}


		#endif



		if (BurstFire) {
			if (isFiring) {
				
				for (int pass = 0; pass < Input.touchCount; pass++) {
					if (Input.touches [pass].fingerId == fingerIndex) {
						if (Input.touches [pass].phase == TouchPhase.Ended) {
							if (isFiring) {
								fingerIndex = -1;
								isFiring = false;
								if (Anim.IsPlaying ("autoShot") || Anim.IsPlaying("shotBurst") || Anim.IsPlaying("Gun01_ShootAuto") || Anim.IsPlaying("Gun03_ShootAuto")  && !Anim.IsPlaying ("reload") || !Anim.IsPlaying("Gun01Reload") || !Anim.IsPlaying("Gun03Reload")) {
									gunScript.StopBurrstFire ();
								}
							}
						}
					}

				}
			} else {
				if (CanFire) {
					/*
					if (Input.GetKeyDown (KeyCode.Mouse0)) {
						FireMachine (false);
					}*/

					if (IsPointerOverUIObject (false)) {
						FireMachine (false);
					}
				}
			}
		}
	}


	void LateUpdate(){
		if (Input.GetKeyDown (KeyCode.Escape)) {			
			if (PauseCanvas.enabled) {
				if (Options.enabled) {
					Options.enabled = false;
					PauseCanvas.transform.FindChild ("Button").GetComponent<Image> ().enabled = true;
					PauseCanvas.transform.FindChild ("Image").GetComponent<Image> ().enabled = true;
					return;
				} else {
					Source.UnPause ();
					if (Shotgun) {
						ShotgunCrack.UnPause ();
					}
					AudioListener.volume = 1;
					Time.timeScale = 1;
				}
			} else {
				Source.Pause ();
				if (Shotgun) {
					ShotgunCrack.Pause ();
				}
				AudioListener.volume = 0;
				Time.timeScale = 0;
			}
			GameCanvas.enabled = !GameCanvas.enabled;

			PauseCanvas.enabled = !PauseCanvas.enabled;
		}
	}


	private bool IsPointerOverUIObject(bool forceCheck) {		
		for (int pass = 0; pass < Input.touchCount; pass++) {
			PointerEventData eventDataCurrentPosition = new PointerEventData (EventSystem.current);

			eventDataCurrentPosition.position = Input.touches [pass].position;

			List<RaycastResult> results = new List<RaycastResult> ();
			EventSystem.current.RaycastAll (eventDataCurrentPosition, results);
			foreach (RaycastResult r in results) {
				if (r.gameObject.name == "FireButton") {
					if (Input.touches [pass].phase == TouchPhase.Began || forceCheck) {
						fingerIndex = Input.touches [pass].fingerId;
						return true;
					}
				}
			}
		}

			return false;
		
	}
	bool FirstReload = true;




	public void Reload(bool ForceReload){
		if (BulletsOnGun < MaxBulletsOnClip) {
			if (!ForceReload) {
				ReloadedShells = 0;
			}

			if (BurstFire || IsGun) {
				CanFire = false;
			
			}
			if (Anim.IsPlaying ("idle") || Anim.IsPlaying ("Gun01Idle") || Anim.IsPlaying ("Gun03Idle") || !Anim.isPlaying || ForceReload) {
				CancelReloadInvoke = false;
				if (BulletsPool > 0) {

					StopAllCoroutines ();

					Material ReticuleMat = Reticule.GetComponent<Renderer> ().material;
					StartCoroutine (ReticuleHit (ReticuleMat));

					int bulletsToLoad = MaxBulletsOnClip - BulletsOnGun;

					if (Anim.IsPlaying ("idle") || !Anim.isPlaying || Anim.IsPlaying ("Gun01Idle") || Anim.IsPlaying ("Gun03Idle")) {
						gunScript.reload ();
					}
					Source.volume = 0.4f;
					if (!Shotgun) {
						Source.clip = GunSound [0];
						Source.PlayDelayed (ReloadSoundDelay);
					}
					if (BulletsPool > bulletsToLoad) {

						if (Shotgun) {
							if (bulletsToLoad > 0) {
								StartCoroutine (ReloadShotgun ());
							}

						} else {

							BulletsOnGun += bulletsToLoad;
							BulletsPool -= bulletsToLoad;
						}

					} else if (BulletsPool > 0) {
						
						if (Shotgun) {
							StartCoroutine (ReloadShotgun ());
						} else {
							BulletsOnGun += BulletsPool;
							BulletsPool = 0;
						}
					}
				}
				if (!Shotgun) {
					BulletOnGunLabel.text = BulletsOnGun.ToString ();
					BulletPoolLabel.text = BulletsPool.ToString ();
				} 
				FirstReload = !ForceReload;
				if (Shotgun && BulletsOnGun + 1 == MaxBulletsOnClip || BulletsPool == 0) {					
					if (!ForceReload) {
						StartCoroutine (CrossFadeToIdle (ReloadTime, true));
					} else {
						StartCoroutine (CrossFadeToIdle (ReloadTime, true));
					}
				}


				if (BurstFire || IsGun) {
					StartCoroutine (Timer (ReloadTime));
				}
			
			}
		}
	}

	IEnumerator CrossFadeToIdle(float timer, bool StopReload){


		yield return new WaitForSeconds (timer);

		yield return new WaitForSeconds (ReloadTime);

		Anim.CrossFade ("idle");
	}

	IEnumerator ReloadShotgun(){
		yield return new WaitForSeconds (ReloadTime);
		
		BulletsOnGun++;
		BulletsPool--;
		ReloadedShells++;
		Source.clip = GunSound [0];
		Source.PlayDelayed (ReloadSoundDelay);
		BulletOnGunLabel.text = BulletsOnGun.ToString ();
		BulletPoolLabel.text = BulletsPool.ToString ();

		if (!CancelReloadInvoke) {
			Reload (true);
		} else {

			Source.Stop ();
			CancelReloadInvoke = false;
			Anim.CrossFade ("reloadStop", ReloadTime / 2);
			StartCoroutine (CrossFadeToIdle (ReloadTime, false));
			StartCoroutine (Timer (TimeBeteweenBurst));
		}
	}

	void CheckAfterReload(){
		if (IsPointerOverUIObject (true)) {
			FireMachine (true);
		}
	}

	int Score = 0;
	public Text WaveIndicator;
	public void AddPoint(){
		Score++;
		if (Score == MaxScore) {
			int Round = ZombieSpawn.GetRound (true);
			MaxScore = Score + Round * 2;
			Round--;
			WaveIndicator.enabled = true;
			WaveIndicator.text = "Wave " + Round + " Complete";
			Invoke ("TurnOffWaveIndicator", 3);
		}
	}

	void TurnOffWaveIndicator(){
		WaveIndicator.text = "";
		WaveIndicator.enabled = false;
	}

	public void Revive(){
		AIMovement[] Zombies = (AIMovement[])FindObjectsOfType<AIMovement> ();
		foreach (AIMovement Z in Zombies) {
			Destroy (Z.gameObject);
		}
		ZombieSpawn.SetNewWaveRules ();

		GameOverCanvas.enabled = false;
		DepthCamera.enabled = true;
		isAlive = true;
		CanFire = true;
		GameCanvas.enabled = true;

		Health = 10;

		BulletsOnGun = thisGun.StartBulletsOnClip;
		BulletOnGunLabel.text = BulletsOnGun.ToString ();
		BulletsPool = thisGun.StartBulletsPool;
		BulletPoolLabel.text = BulletsPool.ToString ();

		AudioListener.volume = 1f;

		MaxScore = Score + ZombieSpawn.Round * 2;

		Time.timeScale = 1;

	}
	int ReloadedShells;
	public Camera DepthCamera;
	public Canvas Options;
	public AudioClip Groan;
	public int Health = 10;
	bool isAlive = true;
	public void RecieveDamage(){
		if (isAlive) {
			Health--;
			Source.PlayOneShot (Groan);
			if (Health <= 0) {
				CanFire = false;
				if (Score > PlayerPrefs.GetInt ("HighScore")) {
					ScoreLabel.text = "New Highscore: " + Score.ToString ();
					PlayerPrefs.SetInt ("HighScore", Score);
					PlayerPrefs.Save ();
				} else {
					ScoreLabel.text = "Score: " + Score.ToString () +"\nHighscore: " +PlayerPrefs.GetInt ("HighScore");
				}
				GameCanvas.enabled = false;
				GameOverCanvas.enabled = true;
				ZombieSpawn.CanSpawn = false;
				DepthCamera.enabled = false;
				Time.timeScale = 0.01f;
				AudioListener.volume = 0.1f;
				AIMovement[] Zombies = (AIMovement[])FindObjectsOfType<AIMovement> ();
				foreach (AIMovement O in Zombies) {
					O.GameOver ();
				}
				isAlive = false;
			}
		}
	}


	void GunPath(){
		Ray Check = new Ray (transform.position, transform.forward);

		RaycastHit Hit;

	
		if (Physics.Raycast (Check, out Hit, Mathf.Infinity, ZombieLayer)) {
			

			if (Hit.collider.gameObject.layer == 8) {
				Material ReticuleMat = Reticule.GetComponent<Renderer> ().material;
				ReticuleMat.color = Color.red;
				StartCoroutine (ReticuleHit (ReticuleMat));

				int Damage = GunDamage;
				if (Hit.collider.name == "Head") {
					Damage = 10000000;
				} 
				Hit.collider.gameObject.GetComponentInParent<ZombieDmg> ().ReceiveDamage (Damage, transform.forward);
			}
		}

	}

	public GameObject Reticule;
	public IEnumerator ReticuleHit(Material ReticuleMat){
		while (ReticuleMat.color != Color.white) {
			ReticuleMat.color = new Color (Mathf.MoveTowards (ReticuleMat.color.r, 1, 0.1f), Mathf.MoveTowards (ReticuleMat.color.g, 1, 0.1f), Mathf.MoveTowards (ReticuleMat.color.b, 1, 0.1f), 1);
			yield return new WaitForSeconds (0.12f);
		}
	}

	public void LoadScene(int scene){
		if (scene == -1) {
			scene = SceneManager.GetActiveScene ().buildIndex;
		}
		SceneManager.LoadScene (scene);
		AudioListener.volume = 1;
		Time.timeScale = 1;
	}


	public void FireMachine(bool ForceFire){
		if (!Anim.IsPlaying ("reload") || ForceFire) {
			Source.volume = 0.9f;
			StopAllCoroutines ();
			Material ReticuleMat = Reticule.GetComponent<Renderer> ().material;
			StartCoroutine (ReticuleHit (ReticuleMat));
			if (CanFire) {
				if (!isFiring) {
					if (ForceFire) {
						gunScript.fire (true);
					} else {
						gunScript.fire (false);
					}

					isFiring = true;
					StartCoroutine (BurstFireTimer (TimeBeteweenBurst));
				}
			}
		}
	}







	bool isFiring;
	public void Fire(){
		bool CanPass = false;
		if (Shotgun) {
			if (Anim.IsPlaying ("reloadStop")) {
				CanPass = true;
			}
			if (Anim.IsPlaying ("reloadCycle") && ReloadedShells > 3) {
				ReloadedShells = 0;
				CancelReloadInvoke = true;
			}
		}

		if (Anim.IsPlaying ("idle") || CanPass) {
			if (BulletsOnGun > 0) {
				if (CanFire) {
					Source.volume = 0.9f;

					Source.PlayOneShot (GunSound [1]);

					if (Shotgun) {						
						ShotgunCrack.PlayDelayed (0.4f);
					}

					
					StopCoroutine ("Timer");
					StartCoroutine (Timer (TimeBeteweenBurst));
					gunScript.fire (false);

					BulletsOnGun--;
					GunPath ();

				}
			} else {
				if (BulletsPool > 0) {
					CancelReloadInvoke = false;
					Reload (false);
				} else {
					Source.PlayOneShot (GunSound [2]);
				}
			}
			BulletOnGunLabel.text = BulletsOnGun.ToString ();
			BulletPoolLabel.text = BulletsPool.ToString ();
		}
	}

	public void AddBullets(int Bullets){
		BulletsPool += Bullets;
		BulletOnGunLabel.text = BulletsOnGun.ToString ();
		BulletPoolLabel.text = BulletsPool.ToString ();
	}

	void OnTriggerEnter(Collider Col){
		if (Col.tag == "AmmoBox") {
			AmmoBox thisBox = (AmmoBox)Col.GetComponent<AmmoBox> ();
			thisBox.RespawnBox ();
			AddBullets (thisBox.Bullets);
		
		}
	}

	IEnumerator BurstFireTimer(float time){
		
		gunScript.fire (false);
		while (isFiring) {
			if (BulletsOnGun > 0) {
				if (CanFire) {
					if (Anim.IsPlaying ("idle")) {
						gunScript.fire (false);
					}
					if (SpecialBurst) {
						Anim.Blend ("Gun03_ShootAuto");
						gunScript.fire (false);
					}
					
					Source.PlayOneShot (GunSound [1]);
					BulletsOnGun--;
					GunPath ();
					BulletOnGunLabel.text = BulletsOnGun.ToString ();
					BulletPoolLabel.text = BulletsPool.ToString ();
				}
			} else if (BulletsPool > 0) {
				gunScript.StopBurrstFire ();
				isFiring = false;
				Reload (true);
			} else {
				gunScript.StopBurrstFire ();
				Source.PlayOneShot (GunSound [2]);
			}
			yield return new WaitForSeconds (time);
			if (SpecialBurst) {				
				Anim.CrossFadeQueued ("Gun03_ShootAuto");
			}
		}

	}

	IEnumerator Timer(float Time){
		CanFire = false;
		yield return new WaitForSeconds (Time);
		CanFire = true;
		if (BurstFire) {
			CheckAfterReload ();
		}

	}
}
