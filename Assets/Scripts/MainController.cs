﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MainController : MonoBehaviour {
	public bool CanTeleport = true;


	TeleportMarkers CurTeleport;
	TeleportMarkers[] TeleportList;

	void Start () {
		FindCurTeleport ();

	}

	float Angle;
	public void FindCurTeleport(){
		
		TeleportList = (TeleportMarkers[])FindObjectsOfType<TeleportMarkers> ();
		for (int pass = 0; pass < TeleportList.Length; pass++) {
			if (TeleportList [pass].gameObject.transform.position == transform.parent.position) {
				CurTeleport = TeleportList [pass];
				break;
			}
		}
	}

	void Update () {
		if (CanTeleport) {
			GuiPos.Clear ();
			for (int pass = 0; pass < CurTeleport.CloseTeleports.Length; pass++) {
				Vector3 relativePos = (CurTeleport.CloseTeleports [pass].transform.position - (transform.parent.position));
				relativePos.y = 0;
				Vector3 ReferenceVec = (transform.position - transform.parent.position);
				ReferenceVec.y = 0;
				Vector3.Normalize (ReferenceVec);
				Vector3.Normalize (relativePos);
				Angle = Vector3.Angle (ReferenceVec, relativePos) * Mathf.Sign(Vector3.Dot(Vector3.up,Vector3.Cross(ReferenceVec,relativePos)));





				Debug.DrawRay (transform.position, relativePos* 20);
			
				if (Mathf.Abs(Angle) < 60) {
					Vector2 Pos = new Vector2 (Screen.width / 2 + (((Screen.width * Angle) / 1.2f) / (100)), 0);

					Pos.x -= (margin * Pos.x) / Screen.width;

					GuiPos.Add (new Rect (Pos, new Vector2(margin, margin)));

				} else if (Mathf.Abs(Angle) < 120) {
					if (Angle < 0) {						
						Angle += 120;
						Vector2 Pos = new Vector2 (0, Screen.height  + (((Screen.height * -Angle) / 1.2f) / 100) * 2 );

						Pos.y -= ((margin * Pos.y) / Screen.height);

						GuiPos.Add(new Rect (Pos, new Vector2(margin, margin)));
					} else {
						Angle -= 120;
						Vector2 Pos = new Vector2 (Screen.width - margin, Screen.height  + (((Screen.height * Angle) / 1.2f) / 100) * 2 );

						Pos.y -= ((margin * Pos.y) / Screen.height);

						GuiPos.Add(new Rect (Pos, new Vector2(margin, margin)));

					}
				} else {
					if (Angle > 0) {
						Angle -= 180;
					} else{
						Angle += 180;
					}
					Vector2 Pos = new Vector2 (Screen.width / 2 + (((Screen.width * -Angle) / 1.2f) / 100) , Screen.height - margin);

					Pos.x -= (margin * Pos.x) / Screen.width;

					GuiPos.Add(new Rect (Pos, new Vector2(margin, margin)));
				}
			}
		}
	}

	List <Rect> GuiPos = new List<Rect>();
	float margin = 0.05f * Screen.width;

	void OnGUI(){
		for (int pass = 0; pass < GuiPos.Count; pass++) {
			if(GUI.Button(GuiPos[pass], "")){
				transform.parent.position = CurTeleport.CloseTeleports [pass].transform.position;
				CurTeleport = (TeleportMarkers)CurTeleport.CloseTeleports [pass].GetComponent<TeleportMarkers> ();
				
			}
		}
	}
}
