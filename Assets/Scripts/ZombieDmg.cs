﻿using UnityEngine;
using System.Collections;

public class ZombieDmg : MonoBehaviour {
	public int Health = 3;
	Animator Anim;
	AIMovement Movement;
	AudioSource Audio;
	public AudioClip[] ZombieSounds;
	AudioClip DefautClip;

	public void GameOver(){
		Audio.loop = false;
		StopAllCoroutines ();
	}
	void Start () {
		Audio = GetComponent<AudioSource> ();
		DefautClip = Audio.clip;
		Anim = GetComponent<Animator> ();
		Movement = (AIMovement)GetComponent<AIMovement> ();

	}
	bool StillAlive = true;


	public void ReceiveDamage(int Damage, Vector3 DirVec){
		if (StillAlive) {
			Health -= Damage;

			if (Health <= 0) {
				GunController.Manager.AddPoint ();
				
				Movement.Dead = true;
				Movement.StopAllCoroutines ();
				GameObject.Destroy (gameObject, 6);
				Anim.SetBool ("Die", true);
	
				Audio.loop = false;
				Audio.clip = ZombieSounds [1];
				Audio.Play ();
				StillAlive = false;
				this.enabled = false;

			} else {
				Audio.clip = ZombieSounds [0];
				Audio.loop = false;
				Audio.Play ();
				StartCoroutine (Timer ());

			}

		}
	}
	public void DealDamage(){
		
	}

	IEnumerator Timer(){
		yield return new WaitForSeconds (0.5f);
		Audio.clip = DefautClip;
		Audio.loop = true;
		Audio.Play ();
	
	}
}
