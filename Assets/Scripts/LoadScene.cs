﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LoadScene : MonoBehaviour {


	public Canvas[] CanvasOrder;
	public Canvas StartCanvas;

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Back ();
		}	
	}

	public void Back(){
		for (int pass = 0; pass < CanvasOrder.Length - 1; pass++) {
			if (CanvasOrder [pass].enabled) {
				CanvasOrder [pass].enabled = !CanvasOrder [pass].enabled;
				CanvasOrder [pass + 1].enabled = !CanvasOrder [pass + 1].enabled;
				return;
			}
		}
		/*
		foreach (Canvas C in DontQuit) {
			if (C.enabled) {
				C.enabled = false;
				StartCanvas.enabled = true;
				return;
			}
		}
*/
		Debug.Log ("QUit");
		Application.Quit ();


	}


	public void Load(int Scene){
		PlayerPrefs.SetInt ("SceneToLoad", Scene + 2);
		PlayerPrefs.Save ();
		SceneManager.LoadScene (7);
	}
}
