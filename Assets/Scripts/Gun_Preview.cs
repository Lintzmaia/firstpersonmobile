﻿using UnityEngine;
using System.Collections;

public class Gun_Preview : MonoBehaviour {

	public AnimationClip[] pumplist;
	public GameObject target;
	int pumpnumber = 0;
	bool drawed = false;
	bool playerview = true;

	Animation Anim;


	public GameObject silencerDisplay;
	void Start () {
		
		Anim = target.GetComponent<Animation> ();
		if (target.gameObject.name == "gunPlayer") {
			Anim.CrossFade ("idle");
			Anim ["reload"].speed = 1f;
			silencerDisplay.GetComponent<Renderer> ().enabled = false;
		} else if (target.gameObject.name == "mp5kPlayer") {
			//Anim ["reload"].speed = 1.5f;
		} else if (target.gameObject.name == "m4animations") {
			Anim ["reload"].speed = 1.5f;
		}
	}

	public void fire (bool CrossFadeQueued) {
		if (target.gameObject.name == "gunPlayer") {
			Anim.Play ("fire2");
			Anim.CrossFade ("idle", 0.4f);
		} else if (target.gameObject.name == "mp5kPlayer") {
			if (CrossFadeQueued) {
				Anim.CrossFadeQueued ("autoShot");
			} else {
				Anim.Play ("autoShot");
			}
		} else if (target.gameObject.name == "m4animations") {
			if (CrossFadeQueued) {
				Anim.CrossFadeQueued ("shotBurst");
			} else {
				Anim.Play ("shotBurst");
			}
		} else if (target.gameObject.name == "Gun01Start") {			
			Anim.Play ("Gun01_ShootAuto");	
		} else if (target.gameObject.name == "Gun03Start") {
			Anim.Play ("Gun03_ShootAuto");	
		} else {			
			StartCoroutine (ShotgunFire ());
		}
	}

	IEnumerator ShotgunFire(){
		Anim.Play("fire");
		yield return new WaitForSeconds(0.3f);
		Anim.Play("pump1");

		yield return new WaitForSeconds(0.8f);
		Anim.Play("idle");

	}

	public void StopBurrstFire(){
		if (target.gameObject.name == "Gun01Start") {
			Anim.Play ("Gun01Idle");
		} else if (target.gameObject.name == "Gun03Start") {
			Anim.Play ("Gun03Idle");
		} else {
			Anim.CrossFade ("idle", 0.4f);
		}
	}

	public void reload () {
		if (target.gameObject.name == "gunPlayer") {
			Anim.Play ("reload");
			Anim.CrossFade ("idle", 7f);

		} else if (target.gameObject.name == "mp5kPlayer") {			
			Anim.Play ("reload");
			//Anim.CrossFade ("idle", 7);

		} else if (target.gameObject.name == "m4animations") {
			//Anim ["reload"].speed = 1.5f;
			Anim.Play ("reload");
			Anim.CrossFade ("idle", 7);
		} else if (target.gameObject.name == "Gun01Start") {
			Anim.CrossFadeQueued ("Gun01Reload");
			Anim.Play ("Gun01Reload");
		} else if (target.gameObject.name == "Gun03Start") {
			Anim.CrossFadeQueued ("Gun03Reload");
			Anim.Play ("Gun03Reload");
		} else {
			Anim.Play("reloadStart");
			Anim.CrossFade("reloadCycle",0.65f);
		
		}
	}



}
