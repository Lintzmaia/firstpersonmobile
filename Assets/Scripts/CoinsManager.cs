﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CoinsManager : MonoBehaviour {
	public static CoinsManager instane;
	public Text[] CoinLabel;
	public bool Clear;
	void Awake(){
		instane = this;
	}

	public int InitialCoinAmount = 20000;


	void Start () {
		if (Clear) {
			PlayerPrefs.DeleteAll ();
		}
		if (PlayerPrefs.GetInt ("Coins", -1) == -1) {
			PlayerPrefs.SetInt ("Coins", InitialCoinAmount);
		}
		foreach (Text L in CoinLabel) {
			L.text = "Coins: " + PlayerPrefs.GetInt ("Coins").ToString ();
		}
	}

	public void UpgradeValue(){
		foreach (Text L in CoinLabel) {
			L.text = "Coins: " + PlayerPrefs.GetInt ("Coins").ToString ();
		}
	}
}
