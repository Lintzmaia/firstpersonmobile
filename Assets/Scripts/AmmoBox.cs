﻿using UnityEngine;
using System.Collections;

public class AmmoBox : MonoBehaviour {
	public int Bullets = 10;
	BoxCollider Bc;
	MeshRenderer Mr;
	AudioSource Audio;
	void Start(){
		Mr = GetComponent<MeshRenderer> ();
		Bc = GetComponent<BoxCollider> ();
		Audio = GetComponent<AudioSource> ();
	}
	public void RespawnBox(){
		Audio.Play ();
		Mr.enabled = false;
		Bc.enabled = false;
		StartCoroutine (Timer ());
	}

	IEnumerator Timer(){
		yield return new WaitForSeconds (10);
		Mr.enabled = true;
		Bc.enabled = true;
	}

}
