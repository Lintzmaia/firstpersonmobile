﻿using UnityEngine;
using System.Collections;

public class DifficultyRule : MonoBehaviour {
	

	public int Difficulty;


	public void SetDifficultyRule(int value){
		Difficulty = value;
		PlayerPrefs.SetInt ("Difficulty", value);
		PlayerPrefs.Save ();
	}


}
