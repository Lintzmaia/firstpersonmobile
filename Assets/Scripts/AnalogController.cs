﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class AnalogController : MonoBehaviour {
	
	public Canvas AnalogCanvas;
	public Image DeadZone;
	public Image Marker;

	public float Speed = 3.6f;

	public LayerMask WallLayer;

	public Scrollbar SpeedScrollBar;
	public Text SpeedLabel;
	/*
	public void AjustSpeed(float Value){
		Speed = 3 + Value * 12;
		PlayerPrefs.SetFloat ("Speed", Speed);
		SpeedLabel.text = Speed.ToString ();
	}
*/
	void Start () {
/*		Speed = PlayerPrefs.GetFloat ("Speed", 6);
		SpeedLabel.text = Speed.ToString ();
		SpeedScrollBar.value = (Speed - 3) / 12;*/
		AnalogCanvas.enabled = false;
		MovementVector = Vector3.zero;
		HasTouched = false;

	}

	bool HasTouched;

	int FingerIndex;

	Vector2 ZeroFingerPos;
	Vector3 ZeroPos;
	int previousCount;
	Vector3 MovementVector;

	void Update () {
		

		if (Input.touchCount > previousCount && Input.touchCount > 0 && !IsPointerOverUIObject()) {
			for (int pass = 0; pass < Input.touchCount; pass++) {
				if (Input.touches [pass].position.x <= Screen.width / 4 && Input.touches [pass].position.y <= Screen.width / 2.3f &&
				    Input.touches [pass].phase == TouchPhase.Began) {
					FingerIndex = Input.touches [pass].fingerId;
					HasTouched = true;
					break;
				}					
			}
		}




		if (HasTouched) {
			for (int pass = 0; pass < Input.touchCount; pass++) {
				if (Input.touches [pass].fingerId == FingerIndex) {	
					if (Input.touches [pass].phase == TouchPhase.Began) {
						ZeroFingerPos = Input.touches [pass].position;
						ZeroPos = new Vector3 (ZeroFingerPos.x, ZeroFingerPos.y, 0);
						AnalogCanvas.enabled = true;
						DeadZone.rectTransform.position = ZeroPos;
						Marker.rectTransform.position = ZeroPos;
						MovementVector = Vector3.zero;						
					}
					if (Input.touches [pass].phase == TouchPhase.Moved) {
						Vector3 FingerPos = Input.touches [pass].position;

						MovementVector = Vector3.ClampMagnitude (new Vector3 (FingerPos.x - ZeroPos.x, 0, FingerPos.y - ZeroPos.y),
							DeadZone.rectTransform.rect.width);

						Marker.rectTransform.position = ZeroPos + new Vector3 (MovementVector.x, MovementVector.z, 0);
					}
					if (Input.touches [pass].phase == TouchPhase.Ended) {
						MovementVector = Vector3.zero;
						AnalogCanvas.enabled = false;
						HasTouched = false;
					}
					break;
				}
			}
		}

		#if UNITY_EDITOR

		float Haxis = Input.GetAxis("Horizontal");
		float Vaxis = Input.GetAxis("Vertical");
		MovementVector = Vector3.ClampMagnitude( new Vector3(Haxis * 120, 0, Vaxis * 120), 120);


		#endif

		Ray Check = new Ray (transform.position, transform.parent.right);
		Ray Check0 = new Ray (transform.position, transform.parent.right * -1);
		Ray Check1 = new Ray (transform.position, transform.parent.right + transform.forward);
		Ray Check2 = new Ray (transform.position, (transform.parent.right * -1) + transform.forward);
		Ray Check3 = new Ray (transform.position, transform.parent.forward * Mathf.Sign(MovementVector.z));
		if (MovementVector.z == 0) {
			Check3.direction = Vector3.zero;
		}

		RaycastHit Hit;
		RaycastHit Hit0;
		RaycastHit Hit1;
		RaycastHit Hit2;
		RaycastHit Hit3;

		if (Physics.Raycast (Check, out Hit, 1, WallLayer)) {
		}
		if (Physics.Raycast (Check0, out Hit0, 1, WallLayer)) {			
		}
		if (Physics.Raycast (Check1, out Hit1, 1, WallLayer)) {
		}
		if (Physics.Raycast (Check2, out Hit2, 1, WallLayer)) {
		}



		if (Physics.Raycast (Check3, out Hit3, 1f, WallLayer)) {
			if (Hit0.collider != null) { 
				if (Hit3.collider.name != Hit0.collider.name) {
					MovementVector = Vector3.zero;
				}
			} else if (Hit.collider != null && MovementVector != Vector3.zero) {
				if (Hit3.collider.name != Hit.collider.name) {
					MovementVector = Vector3.zero;
				}
			} else if (Hit1.collider != null && MovementVector != Vector3.zero) {
				if (Hit3.collider.name != Hit1.collider.name) {
					MovementVector = Vector3.zero;
				}
			} else if (Hit2.collider != null && MovementVector != Vector3.zero) {
				if (Hit3.collider.name != Hit2.collider.name) {
					MovementVector = Vector3.zero;
				}
			}

		}


		transform.parent.Translate (new Vector3 (MovementVector.x / 120, 0, MovementVector.z / 120) * Time.deltaTime * Speed);
		
		previousCount = Input.touchCount;

	}


	private bool IsPointerOverUIObject() {
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}
}
