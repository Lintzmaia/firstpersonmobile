﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FadeOutLogo : MonoBehaviour {

	Image Logo;
	void Start () {
		Logo = GetComponent<Image> ();
		Logo.color = new Color (Logo.color.r, Logo.color.g, Logo.color.b, 1);
		StartCoroutine (FadeOut ());
	}

	IEnumerator FadeOut(){
		yield return new WaitForSeconds (1);
		while (Logo.color.a > 0) {
			Logo.color = new Color (Logo.color.r, Logo.color.g, Logo.color.b, Logo.color.a - 0.03f);
			yield return new WaitForEndOfFrame ();
		}
		yield return new WaitForSeconds (0.3f);
		UnityEngine.SceneManagement.SceneManager.LoadScene (1);
	}

}
