﻿using UnityEngine;
using System;
using System.Collections;

public class Spawn : MonoBehaviour {

	public static Spawn Spawner;

	public GameObject Zombie;
	public Vector3[] Positions;
	[Serializable]
	public class WaveRules
	{
		public int HealthOfZombie;
		public float SpeedOfZombie;
		public void Values(int H, float S){
			HealthOfZombie = H;
			SpeedOfZombie = S;
		}


	}

	public WaveRules[] Rules;

	public WaveRules[] EasyRules;
	public WaveRules[] MediumRules;
	public WaveRules[] HardRules;

	public int Round;
	int NunberOfZombies;
	int MaxNunberOfZombies;

	void Awake(){
		Spawner = this;
	}

	void Start () {
		if (Round <= 0) {
			Round = 1;
		}
		MaxNunberOfZombies = Round * 2;
		//Zombie.gameObject.GetComponent<AIMovement> ().Speed = PlayerPrefs.GetFloat ("Speed");
		/*
		switch (PlayerPrefs.GetInt ("Difficulty")) {
		case 1:
			Rules = EasyRules;
			break;
		case 2:
			Rules = MediumRules;
			break;
		case 3:
			Rules = HardRules;
			break;
		}*/

		Rules = MediumRules;
		StartCoroutine (SpawnZombie ());
	}


	public int GetRound(bool StartNewWave){
		StopAllCoroutines ();
		if (StartNewWave) {
			Round++;
			Invoke ("SetNewWaveRules", 3);
		}
		return Round;
	}

	public void SetNewWaveRules(){
		MaxNunberOfZombies = (Round * 2);
		NunberOfZombies = 0;
		CanSpawn = true;
		StartCoroutine (SpawnZombie ());
	}

	int LastPos;
	public bool CanSpawn = true;
	public float TimeToSpawn = 5;
	IEnumerator SpawnZombie(){
		

		while (CanSpawn) {

			if (NunberOfZombies == MaxNunberOfZombies) {
				CanSpawn = false;
			} else {
				Redo:
				int Index = UnityEngine.Random.Range (0, Positions.Length);
				if (Index == LastPos) {
					goto Redo;
				}
				LastPos = Index;
				GameObject thisZombie = (GameObject)Instantiate (Zombie, Positions [Index], Quaternion.identity);
				if (Round > 20) {
					thisZombie.gameObject.GetComponent<ZombieDmg> ().Health = Rules [Rules.Length - 1].HealthOfZombie;
					thisZombie.gameObject.GetComponent<AIMovement> ().Speed = Rules [Rules.Length - 1].SpeedOfZombie;
				} else {
					thisZombie.gameObject.GetComponent<ZombieDmg> ().Health = Rules [Round - 1].HealthOfZombie;
					thisZombie.gameObject.GetComponent<AIMovement> ().Speed = Rules [Round - 1].SpeedOfZombie;
				}
				NunberOfZombies++;
			}
			yield return new WaitForSeconds (TimeToSpawn);				
		}
	}
}
