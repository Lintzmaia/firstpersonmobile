﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UnlockGun : MonoBehaviour {
	public bool DefaultGun;

	public bool OnPlayScene;
	public Button GunButton;
	public Button UnlockButton;
	public Button UpgradeButton;
	public string UnlockString;
	public string UpgradeString;
	public int GunCost;
	public int UpgradeCost;




	void Start () {
		if (DefaultGun) {
			GunButton.GetComponentInParent<Canvas> ().enabled = false;
			PlayerPrefs.SetInt (UnlockString, 1);
		}

		UpdateStatus ();
	}




	public void UpdateStatus(){

		Debug.Log (GunButton.gameObject.name + " Unlock: " + PlayerPrefs.GetInt (UnlockString, -1));
		Debug.Log (GunButton.gameObject.name + " Upgrade: " + PlayerPrefs.GetInt (UpgradeString, -1));
		if (PlayerPrefs.GetInt (UnlockString, -1) == -1) {

			UnlockButton.gameObject.SetActive (true);

			if (OnPlayScene) {
				GunButton.interactable = false;
				UpgradeButton.gameObject.SetActive (false);
			} else {
				UpgradeButton.interactable = false;
			}

		} else {
			if (OnPlayScene) {
				GunButton.interactable = true;
				UnlockButton.gameObject.SetActive (false);
			} else {
				UnlockButton.interactable = false;
			}

			if (PlayerPrefs.GetInt (UpgradeString, -1) == -1) {
				UpgradeButton.gameObject.SetActive (true);
				UpgradeButton.interactable = true;
			} else {
				if (OnPlayScene) {
					UpgradeButton.gameObject.SetActive (false);
				} else {
					UpgradeButton.interactable = false;
				}
			}
		}
	
	
	}

	public void SavePurchase(){	
	
		if (PlayerPrefs.GetInt (UnlockString, -1) == -1) {
			if (PlayerPrefs.GetInt ("Coins") > GunCost) {
				PlayerPrefs.SetInt (UnlockString, 1);
				if (OnPlayScene) {
					UnlockButton.gameObject.SetActive (false);
					GunButton.interactable = true;
					UpgradeButton.gameObject.SetActive (true);
				} else {
					UnlockButton.interactable = false;
					UpgradeButton.interactable = true;

				}
				PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins") - GunCost);
				CoinsManager.instane.UpgradeValue ();
			}
		} else {
			if (PlayerPrefs.GetInt ("Coins") > UpgradeCost) {
				PlayerPrefs.SetInt (UpgradeString, 1);
				if (OnPlayScene) {
					UpgradeButton.gameObject.SetActive (false);
				} else {
					UpgradeButton.interactable = false;
				}
				PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins") - UpgradeCost);
				CoinsManager.instane.UpgradeValue ();
			}
		}
		PlayerPrefs.Save ();
	}

	// Update is called once per frame
	void Update () {
	
	}
}
