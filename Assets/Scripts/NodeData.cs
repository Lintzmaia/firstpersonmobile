﻿using UnityEngine;
using System.Collections;

public class NodeData : MonoBehaviour {
	public Vector3 BarrierLimit;
	public Vector3 Target;

	public Vector3 BarrierLimit2;
	public Vector3 Target2;

	public int Angle;

	public float AddToTarget;
	public bool twoTargets;
	public bool useTimer;
	public string SecondAxisHandle;
	public float timeToReach;
	public bool FinalTarget;
}
