﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadAsync : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (ScreenTime ());
	}

	IEnumerator ScreenTime(){
		yield return new WaitForSeconds (4.5f);
		SceneManager.LoadSceneAsync (PlayerPrefs.GetInt ("SceneToLoad"));
	}

}
