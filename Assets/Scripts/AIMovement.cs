﻿using UnityEngine;
using System.Collections;

public class AIMovement : MonoBehaviour {
	GameObject Player;
	Transform PlayerTransform;

	public float Speed;
	CharacterController Cc;

	public float DistanceToAttack = 0.3f;

	Animator Anim;

	Vector3 Target;
	public bool FollowPlayer = true;

	LookAtDummy thisLookAt;
	Quaternion DummyRotation;

	bool FinalTarget;

	void Start () {

		thisLookAt = GetComponentInChildren<LookAtDummy> ();
		DummyRotation = thisLookAt.GetComponent<Transform> ().rotation;
		Cc = GetComponent<CharacterController> ();
		Player = GameObject.FindGameObjectWithTag ("Player");
		PlayerTransform = Player.GetComponent<Transform> ();
		Anim = GetComponent<Animator> ();
	}

	IEnumerator DealDamage(){
		IsRunningDmg = true;
		yield return new WaitForSeconds (0.4f);
		GunController.Manager.RecieveDamage ();
		while (true) {
			yield return new WaitForSeconds (0.4f);
			if ((PlayerTransform.transform.position - transform.position).magnitude < DistanceToAttack) {
				GunController.Manager.RecieveDamage ();
			} else {
				break;
			}
			yield return new WaitForSeconds (0.26f);
			if ((PlayerTransform.transform.position - transform.position).magnitude < DistanceToAttack) {
				GunController.Manager.RecieveDamage ();
			} else {
				break;
			}

			yield return new WaitForSeconds (0.43f);
			if ((PlayerTransform.transform.position - transform.position).magnitude < DistanceToAttack) {
				GunController.Manager.RecieveDamage ();
			} else {
				break;
			}

		}
	}

	public void GameOver(){
		Dead = true;
		StopAllCoroutines ();
		GetComponent<ZombieDmg> ().GameOver ();
	}

	IEnumerator CanMoveTimer(){
		yield return new WaitForSeconds (0.8f);
		CanMove = true;
		IsRunningDmg = false;
	}


	bool IsRunningDmg;
	Vector2 moveDir;
	public bool Dead;
	void Update () {
		if (!Dead) {
			if (FollowPlayer) {
				Target = PlayerTransform.transform.position;
				if ((Target - transform.position).magnitude < DistanceToAttack) {
					Anim.SetBool ("Attack", true);
					CanMove = false;

					if (!IsRunningDmg) {
						StopAllCoroutines ();
						StartCoroutine (DealDamage ());

					}
				
				} else {
					Anim.SetBool ("Attack", false);
					if (IsRunningDmg) {						
						StartCoroutine (CanMoveTimer ());

					}
				}
			} else {
				
				if (FinalTarget) {
					if ((Target - transform.position).magnitude < 1.5f) {					
						FollowPlayer = true;
						CheckCol = true;
					}
				}
			}
			Target = new Vector3 (Target.x, transform.position.y, Target.z);

			thisLookAt.LookAt (Target);

			DummyRotation = thisLookAt.gameObject.transform.rotation;
			transform.rotation = Quaternion.Slerp (transform.rotation, DummyRotation, 5 * Time.deltaTime);
			if (CanMove) {			
				moveDir = new Vector2 (Target.x - transform.position.x, Target.z - transform.position.z);
				moveDir.Normalize ();
				Cc.SimpleMove (new Vector3 (moveDir.x, -10, moveDir.y) * Speed);
			} else {
				
			}

		} else {
			Cc.enabled = false;
		}
	}
	public bool CanMove = true;

	public bool CheckCol = true;
	void OnTriggerStay(Collider Col){	
		
		if (Col.tag == "X+Barrier") {
			
			NodeData thisNode = (NodeData)Col.gameObject.GetComponent<NodeData> ();
			if (thisNode.twoTargets) {
				

				if (PlayerTransform.position.x > thisNode.BarrierLimit.x) {

					switch (thisNode.SecondAxisHandle) {
					case "Less":
						
						if (PlayerTransform.position.z < thisNode.BarrierLimit.z) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						} else {
							
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;

					case "More":
						if (PlayerTransform.position.z > thisNode.BarrierLimit.z) {

							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						} else {
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;
					
					}
				} else if (PlayerTransform.position.x < thisNode.BarrierLimit2.x) {
					
					switch (thisNode.SecondAxisHandle) {
					case "Less":
						if (PlayerTransform.position.z < thisNode.BarrierLimit2.z) {

							FollowPlayer = false;
							Target = thisNode.Target2;
							FinalTarget = thisNode.FinalTarget;
						} else {
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;

					case "More":
						if (PlayerTransform.position.z > thisNode.BarrierLimit2.z) {

							FollowPlayer = false;
							Target = thisNode.Target2;
							FinalTarget = thisNode.FinalTarget;
						} else {
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;
					}
				} else {
					switch (thisNode.SecondAxisHandle) {
					case "More":
						if (PlayerTransform.position.z > thisNode.BarrierLimit.z) {

							if (FollowPlayer) {
								
								FollowPlayer = false;
								FinalTarget = thisNode.FinalTarget;
								if ((thisNode.Target - transform.position).magnitude < (thisNode.Target2 - transform.position).magnitude) {
									Target = thisNode.Target;
									if (thisNode.AddToTarget > 0) {
										Target = new Vector3 (Target.x + thisNode.AddToTarget, Target.y, Target.z);
									}
								} else {
									Target = thisNode.Target2;
									if (thisNode.AddToTarget > 0) {
										Target = new Vector3 (Target.x - thisNode.AddToTarget, Target.y, Target.z);
									}

								}
							}
						} else {
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;
					case "Less":
						if (PlayerTransform.position.z < thisNode.BarrierLimit.z) {
							if (FollowPlayer) {
								FollowPlayer = false;
								FinalTarget = thisNode.FinalTarget;

								if ((thisNode.Target - transform.position).magnitude < (thisNode.Target2 - transform.position).magnitude) {
									Target = thisNode.Target;
									if (thisNode.AddToTarget > 0) {
										Target = new Vector3 (Target.x - thisNode.AddToTarget, Target.y, Target.z + thisNode.AddToTarget);
									}
								} else {
									Target = thisNode.Target2;
									if (thisNode.AddToTarget > 0) {
										Target = new Vector3 (Target.x - thisNode.AddToTarget, Target.y, Target.z - thisNode.AddToTarget);
									}
								}
							}
						} else {
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;
					}
				}

			} else if (PlayerTransform.position.x > thisNode.BarrierLimit.x) {
				 
				switch (thisNode.SecondAxisHandle) {
				case "Less":
					if (PlayerTransform.position.z < thisNode.BarrierLimit.z) {
						if (FollowPlayer || !FinalTarget) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						}
					} else {						
						FollowPlayer = true;
						CheckCol = true;
					
					}
					break;

				case "More":
					if (PlayerTransform.position.z > thisNode.BarrierLimit.z) {
						if (FollowPlayer || !FinalTarget) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						}
					} else {
						FollowPlayer = true;
						CheckCol = true;
					
					}
					break;

				case "Any":
					if (FollowPlayer || !FinalTarget) {
						FollowPlayer = false;
						Target = thisNode.Target;
						FinalTarget = thisNode.FinalTarget;
					}
					break;

				case "UseAngle":
					float Angle = Vector2.Angle (new Vector2 (transform.forward.x, transform.forward.z), 
						new Vector2 (thisNode.BarrierLimit.x, thisNode.BarrierLimit.z) - 
						new Vector2 (thisNode.gameObject.transform.position.x, thisNode.gameObject.transform.position.z));


					if (Angle < thisNode.Angle) {
						if (FollowPlayer) {
					
							FollowPlayer = false;
							FinalTarget = thisNode.FinalTarget;
							if (Vector2.Angle (new Vector2 (thisNode.Target.x - transform.position.x, thisNode.Target.z - transform.position.z), transform.forward)
								< Vector2.Angle (new Vector2 (thisNode.Target2.x - transform.position.x, thisNode.Target2.z - transform.position.z),
									transform.forward)) {								
								Target = thisNode.Target;

							} else {

								Target = thisNode.Target2;
							}
						}
					}

					break;
				}

			} else {

				FollowPlayer = true;
				CheckCol = true;
			
			}
		} else if (Col.tag == "X-Barrier") {

			NodeData thisNode = (NodeData)Col.gameObject.GetComponent<NodeData> ();
			if (PlayerTransform.position.x < thisNode.BarrierLimit.x) {

				switch (thisNode.SecondAxisHandle) {
				case "Less":
					if (PlayerTransform.position.z < thisNode.BarrierLimit.z) {
						if (FollowPlayer || !FinalTarget) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						}
					} else {
						FollowPlayer = true;
						CheckCol = true;
					
					}
					break;

				case "More":						
					if (PlayerTransform.position.z > thisNode.BarrierLimit.z) {
						if (FollowPlayer || !FinalTarget) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						}
					} else {
						FollowPlayer = true;
						CheckCol = true;
					
					}
					break;

				case "Any":
					if (FollowPlayer || !FinalTarget) {
						FollowPlayer = false;
						Target = thisNode.Target;
						FinalTarget = thisNode.FinalTarget;
					}
					break;

				case "UseAngle":
					float Angle = Vector2.Angle (new Vector2 (transform.forward.x, transform.forward.z), 
						new Vector2 (thisNode.BarrierLimit.x, thisNode.BarrierLimit.z) - 
						new Vector2 (thisNode.gameObject.transform.position.x, thisNode.gameObject.transform.position.z));


					if (Angle < thisNode.Angle) {
						if (FollowPlayer) {

							FollowPlayer = false;
							FinalTarget = thisNode.FinalTarget;
							if (Vector2.Angle (new Vector2 (thisNode.Target.x - transform.position.x, thisNode.Target.z - transform.position.z), transform.forward)
								< Vector2.Angle (new Vector2 (thisNode.Target2.x - transform.position.x, thisNode.Target2.z - transform.position.z),
									transform.forward)) {								
								Target = thisNode.Target;

							} else {

								Target = thisNode.Target2;
							}
						}
					}

					break;
				}

			} else {

				FollowPlayer = true;
				CheckCol = true;
			
			}
		} else if (Col.tag == "Z+Barrier") {
			
			NodeData thisNode = (NodeData)Col.gameObject.GetComponent<NodeData> ();

			if (thisNode.twoTargets) {
				
				if (PlayerTransform.position.z > thisNode.BarrierLimit.z) {
					
					switch (thisNode.SecondAxisHandle) {
					case "Less":

						if (PlayerTransform.position.x <= thisNode.BarrierLimit.x) {
							
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						} else {
							
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;

					case "More":
						
						if (PlayerTransform.position.x >= thisNode.BarrierLimit.x) {
							
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						} else {
							
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;

					}
				} else if (PlayerTransform.position.z < thisNode.BarrierLimit2.z) {

					switch (thisNode.SecondAxisHandle) {
					case "Less":
						
						if (PlayerTransform.position.x < thisNode.BarrierLimit2.x) {
							
							FollowPlayer = false;
							Target = thisNode.Target2;
							FinalTarget = thisNode.FinalTarget;
						} else {
							
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;

					case "More":
						
						if (PlayerTransform.position.x > thisNode.BarrierLimit2.x) {
							
							FollowPlayer = false;
							Target = thisNode.Target2;
							FinalTarget = thisNode.FinalTarget;
						} else {
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;
					}
				} else {
					switch (thisNode.SecondAxisHandle) {
					case "More":
						
						if (PlayerTransform.position.x > thisNode.BarrierLimit.x) {
							
							if (FollowPlayer) {
								
								FollowPlayer = false;
								FinalTarget = thisNode.FinalTarget;
								if ((thisNode.Target - transform.position).magnitude < (thisNode.Target2 - transform.position).magnitude) {
									Target = thisNode.Target;
									if (thisNode.AddToTarget > 0) {
										Target = new Vector3 (Target.x + thisNode.AddToTarget, Target.y, Target.z + thisNode.AddToTarget);
									}
								} else {
									Target = thisNode.Target2;
									if (thisNode.AddToTarget > 0) {
										Target = new Vector3 (Target.x - thisNode.AddToTarget, Target.y, Target.z + thisNode.AddToTarget);
									}
								}
							}
					
						} else {
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;
					case "Less":
						if (PlayerTransform.position.x < thisNode.BarrierLimit.x) {
							
							if (FollowPlayer) {
								
								FollowPlayer = false;
								FinalTarget = thisNode.FinalTarget;
								if ((thisNode.Target - transform.position).magnitude < (thisNode.Target2 - transform.position).magnitude) {
									Target = thisNode.Target;
									if (thisNode.AddToTarget > 0) {
										Target = new Vector3 (Target.x + thisNode.AddToTarget, Target.y, Target.z - thisNode.AddToTarget);
									}
								} else {
									Target = thisNode.Target2;
									if (thisNode.AddToTarget > 0) {
										Target = new Vector3 (Target.x - thisNode.AddToTarget, Target.y, Target.z - thisNode.AddToTarget);
									}

								}
							}
						} else {
							FollowPlayer = true;
							CheckCol = true;
						
						}
						break;
					}
				}




			} else if (PlayerTransform.position.z > thisNode.BarrierLimit.z) {
					
				switch (thisNode.SecondAxisHandle) {
				case "Less":
					if (PlayerTransform.position.x < thisNode.BarrierLimit.x) {
						if (FollowPlayer || !FinalTarget) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						}
					} else {
						FollowPlayer = true;
						CheckCol = true;
												
					}
					break;

				case "More":
					if (PlayerTransform.position.x > thisNode.BarrierLimit.x) {
						if (FollowPlayer || !FinalTarget) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						}
					} else {
						FollowPlayer = true;
						CheckCol = true;
												
					}
					break;

				case "Any":
					if (FollowPlayer || !FinalTarget) {
						FollowPlayer = false;
						Target = thisNode.Target;
						FinalTarget = thisNode.FinalTarget;
					}
					break;

				case "UseAngle":
					float Angle = Vector2.Angle (new Vector2 (transform.forward.x, transform.forward.z), 
						new Vector2 (thisNode.BarrierLimit.x, thisNode.BarrierLimit.z) - 
						new Vector2 (thisNode.gameObject.transform.position.x, thisNode.gameObject.transform.position.z));


					if (Angle < thisNode.Angle) {
						

						if (FollowPlayer) {
							
							FollowPlayer = false;
							FinalTarget = thisNode.FinalTarget;
							if (Vector2.Angle (new Vector2 (thisNode.Target.x - transform.position.x, thisNode.Target.z - transform.position.z), transform.forward)
								< Vector2.Angle (new Vector2 (thisNode.Target2.x - transform.position.x, thisNode.Target2.z - transform.position.z),
									transform.forward)) {								
								Target = thisNode.Target;

							} else {

								Target = thisNode.Target2;
							}
						}
					}

					break;

				}

			} else {

				FollowPlayer = true;
				CheckCol = true;
			
			}

		} else if (Col.tag == "Z-Barrier") {
			
			NodeData thisNode = (NodeData)Col.gameObject.GetComponent<NodeData> ();

			if (PlayerTransform.position.z < thisNode.BarrierLimit.z) {
				
					
				switch (thisNode.SecondAxisHandle) {

				case "Less":
					if (PlayerTransform.position.x < thisNode.BarrierLimit.x) {
						if (FollowPlayer || !FinalTarget) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						}
					} else {
						FollowPlayer = true;
						CheckCol = true;
					
					}					
					break;

				case "More":
					if (PlayerTransform.position.x > thisNode.BarrierLimit.x) {
						if (FollowPlayer || !FinalTarget) {
							FollowPlayer = false;
							Target = thisNode.Target;
							FinalTarget = thisNode.FinalTarget;
						}
					} else {
						FollowPlayer = true;
						CheckCol = true;
					
					}
					break;

				case "Any":
					if (FollowPlayer || !FinalTarget) {
						FollowPlayer = false;
						Target = thisNode.Target;
						FinalTarget = thisNode.FinalTarget;
					}
					break;

				case "UseAngle":
					float Angle = Vector2.Angle (new Vector2 (transform.forward.x, transform.forward.z), 
						new Vector2 (thisNode.BarrierLimit.x, thisNode.BarrierLimit.z) - 
						new Vector2 (thisNode.gameObject.transform.position.x, thisNode.gameObject.transform.position.z));


					if (Angle < thisNode.Angle) {

						if (FollowPlayer) {							
							FollowPlayer = false;
							FinalTarget = thisNode.FinalTarget;
							if (Vector2.Angle (new Vector2 (thisNode.Target.x - transform.position.x, thisNode.Target.z - transform.position.z), transform.forward)
								< Vector2.Angle (new Vector2 (thisNode.Target2.x - transform.position.x, thisNode.Target2.z - transform.position.z),
									transform.forward)) {								
								Target = thisNode.Target;

							} else {
								
								Target = thisNode.Target2;
							}
						}
					}

					break;
				}

			} else {
				
				FollowPlayer = true;
				CheckCol = true;
			
			}
		}


	}


	/*IEnumerator ColTimer(float Time){
		CheckCol = false;
		yield return new WaitForSeconds (Time);

		CheckCol = true;		

	}*/
}
