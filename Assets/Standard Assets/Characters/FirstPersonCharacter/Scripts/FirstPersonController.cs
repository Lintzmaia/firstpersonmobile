using System;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (CharacterController))]
    [RequireComponent(typeof (AudioSource))]
    public class FirstPersonController : MonoBehaviour
    {


        [SerializeField] private bool m_IsWalking;
        [SerializeField] private float m_WalkSpeed;
        [SerializeField] private float m_RunSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;      
        [SerializeField] private float m_StepInterval;
        [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
        [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

     
        private bool m_Jump;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        private AudioSource m_AudioSource;



        // Use this for initialization
        private void Start()
        {
			
            m_CharacterController = GetComponent<CharacterController>();
            m_StepCycle = 0f;

		//	Gun = (GunController)GetComponentInChildren<GunController> ();

            m_NextStep = m_StepCycle/2f;
            m_Jumping = false;
            m_AudioSource = GetComponent<AudioSource>();
        }

		private bool IsPointerOverUIObject() {
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
			eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
			return results.Count > 0;
		}




        // Update is called once per frame
        private void Update()
        {          
            
            if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
            {
                m_MoveDir.y = 0f;
            }

            m_PreviouslyGrounded = m_CharacterController.isGrounded;
        }


        private void PlayLandingSound()
        {
            m_AudioSource.clip = m_LandSound;
            m_AudioSource.Play();
            m_NextStep = m_StepCycle + .5f;
        }

		Vector2 ZeroFingerPos;
		Vector3 ZeroPos;
		int previousCount;
		Vector3 MovementVector;

		bool HasTouched;

		int FingerIndex;



		public Canvas AnalogCanvas;
		public Image DeadZone;
		public Image Marker;


        private void FixedUpdate()
        {
			if (Input.touchCount > previousCount && Input.touchCount > 0 && !IsPointerOverUIObject()) {
				for (int pass = 0; pass < Input.touchCount; pass++) {
					if (Input.touches [pass].position.x <= Screen.width / 4 && Input.touches [pass].position.y <= Screen.width / 2.3f &&
						Input.touches [pass].phase == TouchPhase.Began) {
						FingerIndex = Input.touches [pass].fingerId;
						HasTouched = true;
						break;
					}					
				}
			}




			if (HasTouched) {
				for (int pass = 0; pass < Input.touchCount; pass++) {
					if (Input.touches [pass].fingerId == FingerIndex) {	
						if (Input.touches [pass].phase == TouchPhase.Began) {
							ZeroFingerPos = Input.touches [pass].position;
							ZeroPos = new Vector3 (ZeroFingerPos.x, ZeroFingerPos.y, 0);
							AnalogCanvas.enabled = true;
							DeadZone.rectTransform.position = ZeroPos;
							Marker.rectTransform.position = ZeroPos;
							MovementVector = Vector3.zero;						
						}
						if (Input.touches [pass].phase == TouchPhase.Moved) {
							Vector3 FingerPos = Input.touches [pass].position;

							MovementVector = Vector3.ClampMagnitude (new Vector3 (FingerPos.x - ZeroPos.x, 0, FingerPos.y - ZeroPos.y),
								DeadZone.rectTransform.rect.width);

							Marker.rectTransform.position = ZeroPos + new Vector3 (MovementVector.x, MovementVector.z, 0);
						}
						if (Input.touches [pass].phase == TouchPhase.Ended) {
							MovementVector = Vector3.zero;
							AnalogCanvas.enabled = false;
							HasTouched = false;
						}
						break;
					}
				}
			}
			float speed;
			GetInput(out speed);
			#if UNITY_EDITOR
			MovementVector = new Vector3(m_Input.x, 0, m_Input.y);
			#endif

            // always move along the camera forward as it is the direction that it being aimed at
			Vector3 desiredMove = transform.forward*MovementVector.z + transform.right*MovementVector.x;

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                               m_CharacterController.height/2f, ~0, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            m_MoveDir.x = desiredMove.x*speed;
            m_MoveDir.z = desiredMove.z*speed;


            if (m_CharacterController.isGrounded)
            {
                m_MoveDir.y = -m_StickToGroundForce;

            }
            else
            {
                m_MoveDir += Physics.gravity*m_GravityMultiplier*Time.fixedDeltaTime;
            }
            m_CollisionFlags = m_CharacterController.Move(m_MoveDir*Time.fixedDeltaTime);

            ProgressStepCycle(speed);
          
			previousCount = Input.touchCount;

        }


        private void PlayJumpSound()
        {
            m_AudioSource.clip = m_JumpSound;
            m_AudioSource.Play();
        }


        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed*(m_IsWalking ? 1f : m_RunstepLenghten)))*
                             Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }


        private void PlayFootStepAudio()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }



        private void GetInput(out float speed)
        {
            // Read input
			float horizontal = Input.GetAxis("Horizontal");
			float vertical = Input.GetAxis("Vertical");

          

#if !MOBILE_INPUT
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
            // set the desired speed to be walking or running
            speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
            m_Input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }			   
        }
	

       


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }
    }
}
